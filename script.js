let courses = [

	{
		id: "1",
		name: "Python 101",
		description: "Learn the basics of python.",
		price: 15000,
		isActive: true,
	},
	{
		id: "2",
		name: "CSS 101",
		description: "Learn the basics of CSS.",
		price: 10500,
		isActive: true,

	},
	{
		id: "3",
		name: "CSS 102",
		description: "Learn an advanced CSS.",
		price: 15000,
		isActive: false

	},
	{
		id: "4",
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its Laravel framework.",
		price: 20000,
		isActive: true

	}

]

let addCourse = (id, name, description, price, isActive) => {
	courses.push(
	{
	 "id": id,
	 "name": name,
	 "description": description,
	 "price": price,
	 "isActive": isActive,

	}
		);
	
	console.log(`You have created ${name}. Its price is ${price}.`);
	
}

let getSingleCourse = (id) => {
	let found = courses.find((x) => x.id == id);
	console.log(found);
}

let getAllCourses = () => console.log(courses);

let archiveCourse = (index) => {
	courses[index].isActive = false;
	console.log(courses[index]);
}

let deleteCourse = () => courses.pop();


let showActiveCourses = () => {
	let activeCourses = courses.filter(
		(x) => x.isActive == true 
		);
	console.log(activeCourses);
}